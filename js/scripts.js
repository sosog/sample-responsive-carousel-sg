

var sampleCarousel = (function(){

	var item = $('.carousel .item');
	var bullet = $('.controller-bullet');

	//initialize script
	function init(){

		//setBulets();
	}

	//set bullets for controller navigation
	function setBulets(){
		var itemCount = item.length;
		var bullets = '';
		for( i=0; itemCount > i; i++){
			bullets = bullets + '<div class="controller-bullet"></div>';
		}

		$('.controller-bullets').html(bullets);
	}


	function moveSlide(n){
		item.removeClass('disabled active');
		item.eq(n).addClass('active');
		item.eq(n + 1).addClass('disabled');


	}

	$('.controller-bullet').on('click',function(){
		moveSlide( $(this).index() );

	})


	return {
		init: function() {
			return init();
		}
	}
}(). init());